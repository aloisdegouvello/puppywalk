let getDuration =
    let twoYears = 24
    let twoHours = 120
    let fiftyYears = 50*12 // is this a constant?

    function
    | x when x <= 0 -> 0
    | x when x <= twoYears -> x * 5
    | x when x < fiftyYears -> twoHours
    | _ -> 0

getDuration 3 = 15
getDuration 4 = 20
getDuration 600 = 0
getDuration 23 = 115
getDuration 24 = 120
getDuration 25 = 120
# PuppyWalk

Puppy Walk is a quick kata for beginner.

![walking dog](https://cdn.dribbble.com/users/59947/screenshots/3344390/worshond_dribbble.gif)



## How long should I walk my puppy?

There is a 5 minute rule which is basically 5 minutes walk per day,
per month of your puppy’s life, so therefore a 3 month old puppy can be walked
for 15 minutes and a 4 month old puppy for 20 minutes, which is a guideline
but as an owner make your own judgement on your puppies needs as each puppy is different.
When he celebrates his secund birthday a puppy is now a dog.
A dog should walk at least two hours per day. Most dog doesnt lives more than 20 years.
You should not walk a dead dog. Zombie dog doesnt exist.

Write a function returning the duration in minutes you should walk your dog given its age in month.


## Source

* Struttin' Sausage! by MUTI 
* [How long and often should I walk my pig?](https://pets.stackexchange.com/questions/626/how-often-and-long-should-i-walk-my-puppy)